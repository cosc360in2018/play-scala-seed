# play-scala-seed

This is a seed project based on a giter8 template.

To run the project, assuming you have sbt installed
(it is installed for you on turing), cd into its top-level directory and
run: 


```sh
sbt
```

Then, from the sbt prompt, to start it on port 1234

```sh
run 1234
```

If you are on a shared machine (eg, turing) you should
choose a port number nobody else is using. On turing, 
there is a script `my_tomcat_ports` that can give you
some random port numbers to use.

To stop the server, press Ctrl-d

To exit the sbt prompt, press Ctrl-d again

### Changes to the giter8 template

I have made some changes to the 

* **Added Hopper as a repository**  
  The Scala Build Tool (sbt) knows how to fetch libraries for you.
  I have added Artifactory on Hopper as a source of libraries.
  
* **Changed the Content-Security-Policy**  
  Out-of-the-box, Play has strict security settings, including setting
  the Content-Security-Policy header to 'self' which might prevent your
  pages from loading other scripts. I've relaxed this.
  
* **Added Play JSON**  
  I have added the library import for the JSON parsing library,
  and a couple of examples
  
